package com.epam.model;

public class ConstMyException {
    public static final String INTERRUPTED_EXCEPTION =
            "\nInterruptedException...\n\n";

    public static final String NULL_POINTER_EXCEPTION =
            "Object can't be null!\nNullPointerException...\n\n";

    public static final String EXECUTION_EXCEPTION =
            "Something wrong with execution this task\nExecutionException...\n\n";

    public static final String ILLEGAL_ARGUMENT_EXCEPTION =
            "Wrong argument, try again please!\nIllegalArgumentException...\n\n";

    public static final String EXCEPTION =
            "Program crushed..\nException...\n\n";

    public static final String CORRECT_POINT =
            "Please choose correct point!";

    private ConstMyException() {
    }
}
