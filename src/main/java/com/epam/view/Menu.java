package com.epam.view;

import com.epam.controller.task1;
import com.epam.controller.task2.task2;
import com.epam.controller.task3.task3;
import com.epam.controller.task4.task4;
import com.epam.controller.task5;
import com.epam.controller.task6.task6;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

import static com.epam.model.ConstMenu.*;
import static com.epam.model.ConstMyException.CORRECT_POINT;
import static com.epam.model.ConstMyException.NULL_POINTER_EXCEPTION;

public class Menu {
    private static final Logger LOG = LogManager.getLogger(Menu.class);
    private static final Scanner INPUT = new Scanner(System.in);

    private Map<String, String> menuDescriptions;
    private Map<String, Functional> menuMethods;

    private Menu() {
        initMenu();
    }

    private void initMenu() {
        menuDescriptions = new LinkedHashMap<>();
        menuMethods = new LinkedHashMap<>();

        menuDescriptions.put("1", POINT_ONE);
        menuDescriptions.put("2", POINT_TWO);
        menuDescriptions.put("3", POINT_THREE);
        menuDescriptions.put("4", POINT_FOUR);
        menuDescriptions.put("5", POINT_FIVE);
        menuDescriptions.put("6", POINT_SIX);
        menuDescriptions.put("0", POINT_EXIT);

        menuMethods.put("1", task1::run);
        menuMethods.put("2", task2::run);
        menuMethods.put("3", task3::run);
        menuMethods.put("4", task4::run);
        menuMethods.put("5", task5::run);
        menuMethods.put("6", task6::run);

        menuMethods.put("0", this::exitFromProgram);
    }

    private void showMenuDescriptions() {
        menuDescriptions.values().forEach(LOG::info);
    }

    private void exitFromProgram(){
        System.exit(0);
    }

    public static void runMenuMain() {
        String keyForRunMenu;
        Menu menuMain = new Menu();
        while (true) {
            try {
                menuMain.showMenuDescriptions();
                LOG.info("Please select menu point.");
                keyForRunMenu = INPUT.nextLine();
                menuMain.menuMethods.get(keyForRunMenu).run();
            } catch (NullPointerException e) {
                LOG.error(CORRECT_POINT);
                LOG.error(NULL_POINTER_EXCEPTION);
            }
        }
    }
}