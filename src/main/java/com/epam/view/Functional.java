package com.epam.view;

@FunctionalInterface
public interface Functional {
    void run();
}