package com.epam.controller.task3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.epam.controller.task2.fibonacci.getFibonacciDigitByIndex;
import static com.epam.model.ConstMyException.INTERRUPTED_EXCEPTION;
import static com.epam.model.ConstPrimitives.COUNT_FIRST_FIBONACCI_DIGIT;
import static com.epam.model.ConstPrimitives.FOUR_THOUSAND;

public class MyScheduledExecutor {
    private static final Logger LOG = LogManager.getLogger(MyScheduledExecutor.class);

    private Runnable createRunnableWithFibonacci() {
        return () -> {
            for (int i = 1; i <= COUNT_FIRST_FIBONACCI_DIGIT; i++) {
                LOG.info(getFibonacciDigitByIndex(i));
            }
        };
    }

    public void runWithScheduledExecutorService() {
        try {
            LOG.debug("runWithScheduledExecutorService");
            ScheduledExecutorService singleExecutor =
                    Executors.newSingleThreadScheduledExecutor();
            Runnable task = createRunnableWithFibonacci();
            int initialDelay = 0;
            int period = 2;
            singleExecutor.scheduleWithFixedDelay(task, initialDelay, period, TimeUnit.SECONDS);
            Thread.sleep(FOUR_THOUSAND);
            singleExecutor.shutdownNow();
        } catch (InterruptedException e) {
            LOG.error(INTERRUPTED_EXCEPTION);
        }
    }
}