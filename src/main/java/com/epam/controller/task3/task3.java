package com.epam.controller.task3;

import static com.epam.model.ConstPrimitives.AMOUNT_THREADS;

public class task3 {

    public static void run() {
        MyExecutor executorService = new MyExecutor();
        executorService.runExecutorService(AMOUNT_THREADS);

        MyScheduledExecutor scheduledExecutor = new MyScheduledExecutor();
        scheduledExecutor.runWithScheduledExecutorService();
    }
}