package com.epam.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.epam.model.ConstMyException.INTERRUPTED_EXCEPTION;
import static com.epam.model.ConstObjects.*;
import static com.epam.model.ConstPrimitives.HUNDRED;
import static com.epam.model.ConstPrimitives.SIZE_OF_CYCLE;


public class task1 {
    private static final Logger LOG = LogManager.getLogger(System.in);

    private volatile boolean isFirstStart;

    private task1() {
    }

    private Thread getWithFirstWait(String outputMessage) {
        return new Thread(() -> {
            synchronized (MONITOR_FOR_SYNC) {
                for (int i = 0; i < SIZE_OF_CYCLE; i++) {
                    try {
                        isFirstStart = true;
                        MONITOR_FOR_SYNC.wait();
                    } catch (InterruptedException ignored) {
                    }
                    LOG.info(outputMessage);
                    MONITOR_FOR_SYNC.notify();
                }
            }
        });
    }

    private Thread getWithFirstNotify(String outputMessage) {
        return new Thread(() -> {
            synchronized (MONITOR_FOR_SYNC) {
                for (int i = 0; i < SIZE_OF_CYCLE; i++) {
                    MONITOR_FOR_SYNC.notify();
                    LOG.info(outputMessage);
                    try {
                        MONITOR_FOR_SYNC.wait();
                    } catch (InterruptedException ignored) {
                    }
                }
            }
        });
    }

    private void pauseIfFirstNotBegun() {
        if (!isFirstStart) {
            try {
                Thread.sleep(HUNDRED);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void startNewThread(String firstOutputMessage,
                                String secondOutputMessage) {

        this.isFirstStart = false;
        Thread first = getWithFirstWait(firstOutputMessage);
        Thread second = getWithFirstNotify(secondOutputMessage);
        first.start();
        pauseIfFirstNotBegun();
        second.start();
        try {
            first.join();
            second.join();
        } catch (InterruptedException e) {
            LOG.error(INTERRUPTED_EXCEPTION);
        }
    }

    public static void run() {
        task1 pingPong = new task1();
        pingPong.startNewThread(PING_STRING,PONG_STRING);
    }
}