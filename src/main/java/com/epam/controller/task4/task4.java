package com.epam.controller.task4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.Callable;

import static com.epam.model.ConstMyException.EXCEPTION;
import static com.epam.model.ConstPrimitives.COUNT_FIRST_FIBONACCI_DIGIT;

public class task4 {
    private static final Logger LOG = LogManager.getLogger(task4.class);

    public static void run() {
        try {
            FibonacciSum sum = new FibonacciSum();
            int sumWithoutCallable =
                    sum.getSumOfFirstFibonacciDigits(COUNT_FIRST_FIBONACCI_DIGIT);
            Callable<Integer> sumWithCallable =
                    sum.getSumOfFirstFibWithCallable(COUNT_FIRST_FIBONACCI_DIGIT);

            LOG.debug("Sum with callable: " + sumWithCallable.call());
            LOG.debug("Sum without callable: " + sumWithoutCallable+"\n\n");
        } catch (Exception e) {
            LOG.info(EXCEPTION);
        }
    }
}