package com.epam.controller.task4;

import java.util.concurrent.Callable;

import static com.epam.controller.task2.fibonacci.getListOfFirstFibonacciNumbers;

class FibonacciSum {

    Integer getSumOfFirstFibonacciDigits(
            int amountFirstFibonacciNumbers) {
        return getListOfFirstFibonacciNumbers(amountFirstFibonacciNumbers)
                .stream()
                .mapToInt(a -> a)
                .sum();
    }

    Callable<Integer> getSumOfFirstFibWithCallable(
            int amountFirstFibDigits) {
        return () -> getSumOfFirstFibonacciDigits(amountFirstFibDigits);
    }
}
