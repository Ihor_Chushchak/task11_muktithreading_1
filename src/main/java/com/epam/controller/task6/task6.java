package com.epam.controller.task6;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.epam.controller.task6.RunMyNumber.runMyNumber;

public class task6 {
    private static final Logger LOG = LogManager.getLogger(task6.class);

    public static void run() {

        runMyNumber(false);

        runMyNumber(true);
    }
}
