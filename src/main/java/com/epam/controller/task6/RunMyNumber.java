package com.epam.controller.task6;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.epam.model.ConstMyException.INTERRUPTED_EXCEPTION;

public class RunMyNumber {
    private static final Logger LOG = LogManager.getLogger(RunMyNumber.class);

    public static com.epam.controller.task6.MyInterface getIsSync(boolean isSynchronized){
        com.epam.controller.task6.MyInterface task;
        if (isSynchronized) {
            task = new MyNumberSyncronized();
        } else {
            task = new MyNumberNonSyncronized();
        }
        return task;
    }

    public static void runMyNumber(boolean isSynchronized) {
        com.epam.controller.task6.MyInterface task;
        task = getIsSync(isSynchronized);
        Thread first = new Thread(task::setAllElementsValue);
        Thread second = new Thread(task::decreaseAllElements);
        Thread third = new Thread(task::increaseAllElements);
        first.start();
        second.start();
        third.start();
        try {
            first.join();
            second.join();
            third.join();
        } catch (InterruptedException e) {
            LOG.error(INTERRUPTED_EXCEPTION);
        }
        task.showListNumbers();
    }
}
