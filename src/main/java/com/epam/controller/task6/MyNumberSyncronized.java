package com.epam.controller.task6;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

import static com.epam.model.ConstPrimitives.*;

public class MyNumberSyncronized implements MyInterface {
    private static final Logger LOG = LogManager.getLogger(MyNumberSyncronized.class);

    private List<Integer> listNumbers;

    public MyNumberSyncronized() {
        initList();
    }

    private void initList() {
        listNumbers = new ArrayList<>();
    }

    @Override
    public synchronized void setAllElementsValue() {
        while (listNumbers.size() != SIZE_LIST_NUMBERS) {
            listNumbers.add(ALL_ELEMENTS_VALUE);
        }
    }

    @Override
    public synchronized void decreaseAllElements() {
        listNumbers.forEach(a ->
                listNumbers.set(listNumbers.lastIndexOf(a),
                        a -= ALL_ELEMENTS_DECREASE));
    }

    @Override
    public synchronized void increaseAllElements() {
        listNumbers.forEach(a ->
                listNumbers.set(listNumbers.indexOf(a),
                        a -= ALL_ELEMENTS_INCREASE));
    }

    @Override
    public void showListNumbers() {
        LOG.debug("\n\nSynchronized list, all elements must be 10!");
        listNumbers.forEach(LOG::debug);
    }
}
