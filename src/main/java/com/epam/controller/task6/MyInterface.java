package com.epam.controller.task6;

public interface MyInterface {

    void setAllElementsValue();

    void decreaseAllElements();

    void increaseAllElements();

    void showListNumbers();
}
