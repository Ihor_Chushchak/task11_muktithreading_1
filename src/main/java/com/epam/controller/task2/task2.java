package com.epam.controller.task2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

import static com.epam.controller.task2.fibonacci.getListFibonacciNumbersFromToEnd;
import static com.epam.controller.task2.fibonacci.getListOfFirstFibonacciNumbers;
import static com.epam.model.ConstMyException.INTERRUPTED_EXCEPTION;
import static com.epam.model.ConstPrimitives.AMOUNT_THREADS;
import static com.epam.model.ConstPrimitives.CONVERT_NANO_TO_SEC;
import static com.epam.model.ConstPrimitives.COUNT_FIRST_FIBONACCI_DIGIT;


public class task2 {
    private static final Logger LOG =
            LogManager.getLogger(task2.class);
    private int amountFibDigits;

    private task2(int amountFibDigits) {
        this.amountFibDigits = amountFibDigits;
    }

    private void showFibWithoutThreads() {
        LOG.debug("ShowFibWithoutThreads");
        long startTime = System.nanoTime();
        getListOfFirstFibonacciNumbers(amountFibDigits).forEach(LOG::info);
        long endTime = System.nanoTime();
        double executionTime = (endTime - startTime * 1.) / CONVERT_NANO_TO_SEC;
        LOG.debug("Execution time: " + executionTime + "\n\n");
    }

    private void showFibThroughCycle(List<Thread> listThreads,
                                     int amountThreads) {
        for (int i = 1; i <= amountFibDigits; i += amountThreads) {
            int finalI = i;
            Thread thread = new Thread(() ->
                    LOG.info(getListFibonacciNumbersFromToEnd(
                            finalI, finalI + amountThreads)));
            listThreads.add(thread);
        }
    }

    private int correctThread(int amountThreads) {
        if (amountThreads < 1 || amountFibDigits < 1) {
            return 0;
        }
        while (amountFibDigits % amountThreads != 0) {
            amountThreads--;
        }
        return amountThreads;
    }

    private void showFibWithThreads(
            int amountThreads) throws InterruptedException {
        amountThreads = correctThread(amountThreads);
        if (amountThreads == 0) {
            return;
        }
        LOG.debug("ShowFibWithThreads: " + amountThreads);
        List<Thread> listThreads = new LinkedList<>();
        long startTime = System.nanoTime();
        showFibThroughCycle(listThreads, amountThreads);
        listThreads.forEach(Thread::start);
        for (Thread listThread : listThreads) {
            listThread.join();
        }
        long endTime = System.nanoTime();
        double executionTime = (endTime - startTime * 1.) / CONVERT_NANO_TO_SEC;
        LOG.debug("Execution time: " + executionTime + "\n\n");
    }

    private void showFibWithZeroThreadsSimple(int step) {
        step = correctThread(step);
        if (step == 0) {
            return;
        }
        LOG.debug("ShowFibWithZeroThreadByCycle: " + step);
        long startTime = System.nanoTime();
        for (int i = 1; i <= amountFibDigits; i += step) {
            LOG.info(getListFibonacciNumbersFromToEnd(i, i + step));
        }
        long endTime = System.nanoTime();
        double executionTime = (endTime - startTime * 1.) / CONVERT_NANO_TO_SEC;
        LOG.debug("Execution time: " + executionTime + "\n\n");
    }

    public static void run() {
        try {
            task2 runTask =
                    new task2(COUNT_FIRST_FIBONACCI_DIGIT);
            runTask.showFibWithoutThreads();
            runTask.showFibWithThreads(AMOUNT_THREADS);
            runTask.showFibWithZeroThreadsSimple(AMOUNT_THREADS);
        } catch (InterruptedException e) {
            LOG.error(INTERRUPTED_EXCEPTION);
        }
    }
}