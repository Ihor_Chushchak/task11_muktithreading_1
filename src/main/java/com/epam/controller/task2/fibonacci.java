package com.epam.controller.task2;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class fibonacci {

    private fibonacci() {
    }

    private static boolean isFibonacci(final int someValue) {
        int first = 1;
        int second = 0;
        for (int i = 0; i < someValue; i++) {
            first += second;
            second = first - second;

            if (first > someValue) {
                return false;
            } else if (first == someValue) {
                return true;
            }
        }
        return false;
    }

    public static List<Integer> getListOfFirstFibonacciNumbers(
            final int countOfFirstDigitsFibonacci) {
        List<Integer> listFibonacciDigits = new LinkedList<>();
        for (int i = 0, amount = 1; amount <= countOfFirstDigitsFibonacci; i++) {
            if (isFibonacci(i)) {
                listFibonacciDigits.add(i);
                amount++;
            }
        }
        return listFibonacciDigits;
    }

    public static Integer getFibonacciDigitByIndex(int index) {
        if (index <= 0) {
            return 0;
        }
        for (int i = 0, amount = 0; ; i++) {
            if (isFibonacci(i)) {
                amount++;
                if (amount == index) {
                    return i;
                }
            }
        }
    }

    private static boolean isNotCorrect(final int first, final int second) {
        return (first > second) || (first < 0);
    }

    public static List<Integer> getListFibonacciNumbersFromToEnd(
            final int from,
            final int to) {

        if (isNotCorrect(from, to)) {
            return new ArrayList<>();
        }
        List<Integer> listFibonacciDigits = new LinkedList<>();
        for (int i = from; i < to; i++) {
            listFibonacciDigits.add(getFibonacciDigitByIndex(i));
        }
        return listFibonacciDigits;
    }
}